###############################################################################
#
# Copyright (C) 2021-2023, james woodyatt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#   Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

### Prerequisites
open Conjury/Basis/HostOS
open Conjury/Packaging/OPAM
open Conjury/V3/Language/OCaml/External
open Conjury/V3/Language/OCaml/Program

### Find dependent packages
OPAM.RequirePackagesInstalled(ounit2)

### Create an OCaml test program using OUnit
OCaml_ounit_test_program(~name, ?tools=$(OCaml_toolchain))=
    where=$"OCaml_ounit_test_program"
    AssertInstance(~obj=$(tools), ~class=$"OCaml_toolchain", ~where=$(where))
    tools.OPTIONS+=$(array -w -3)

    exe=$(OCaml_program ~name=$(name), ~tools=$(tools))

    private.=
        name=$(name)
        tools=$(exe.TOOLS)

    libs[]=
        $(OCamlPackage.Find $"+unix")
        $(OCamlPackage.Find $"+threads")
        $(OCamlPackage.Find $"ounit2.threads")
        $(OCamlPackage.Find $"ounit2.advanced")
        $(OCamlPackage.Find $"ounit2")

    exe=$(exe.UsingExternals $(libs))
    
    exe.+=
        ARGS=$(EMPTY)

        private.compile-junit(runtime)=
            program=$(this.PhaseDepends $(runtime))
            stage=$(tools.STAGE.Push $(HostOS.MkPath run $(runtime)))
            junit=$(stage.Ref $(addsuffix $".junit", $(name)))
            cache=$(stage.Ref $(addsuffix $".cache", $(name)))
            args=$(ARGS)
            export args
            args+=$"-output-junit-file" $(junit)
            args+=$"-cache-filename" $(cache)
            args+=$"-no-output-file"
            if $(VERBOSE)
                args+=$"-verbose" $"true"

            $(junit): $(program) $(stage.DIR) :effects: $(cache)
                $(program) $(args)

            test: $(junit)

        ComposeTestResults()=
            this=$(compose-aux true)
            
            compile-junit(bytecode)
            if $(TOOLS.NATIVE_SUPPORTED)
                compile-junit(native)
            
            return $(this)

    return $(exe)

### End
