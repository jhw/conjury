##############################################################################
#
# Copyright (C) 2021-2023, james woodyatt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#   Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

### Open library modules
open Conjury/Basis/Configurable
open Conjury/Basis/Driver
open Conjury/Basis/Exceptions
open Conjury/Basis/HostOS
open Conjury/Basis/Mode
open Conjury/Basis/Probe

open Conjury/V3/Language/C/Toolchain

### Select driver tool
private.select-tool(name)=
    .STATIC: :key: $(name)
        if $(VERBOSE)
            print($"--- Selecting OCaml tool $(name) -> ")
        optname=$(addsuffix $".opt", $(name))
        tool=
            if $(exists-in-path $(optname))
                value $(optname)
            elseif $(exists-in-path $(name))
                value $(name)
            else
                value $(EMPTY)
        if $(VERBOSE)
            if $(tool)
                println($(tool))
            else
                println($"<UNAVAILABLE>")
    return $(tool)

### Get all the configuration parameters for a compiler
private.query-all-parameters(compiler)=
    .STATIC: :key: $(compiler)
        text=$(shella $(compiler) "-config")

    return $(text)

### Get the named configuration parameter for a compiler
private.query-parameter(compiler, parameter)=
    key=$"$(compiler)-$(parameter)"

    .STATIC: :key: $(key)
        text=$(query-all-parameters $(compiler))
        if $(VERBOSE)
            print($"--- Querying $(compiler) -config $(parameter) --> ")
        search()=
            foreach (line => ..., $(text))
                match $(line)
                case $"^$(parameter): +\(.*\)"
                    ret=$1
                    return $(ret)
            private.m=$"OCaml: no parameter '$(parameter)' available"
            InvalidArgumentException($m)
        ret=$(search)
        if $(VERBOSE)
            println($(ret))

    return $(ret)

### OCaml compiler toolchain
OCaml_toolchain.=
    extends $(Driver)
    class OCaml_toolchain

    DRIVER.=
        BYTECODE=
            drv=$`(select-tool $"ocamlc")
            value $(Configurable ~name=$"OCAMLC", ~default=$(drv))
        NATIVE=
            drv=$`(select-tool $"ocamlopt")
            value $(Configurable ~name=$"OCAMLOPT", ~default=$(drv))
        MKLIB=
            drv=$`(select-tool $"ocamlmklib")
            value $(Configurable ~name=$"OCAMLMKLIB", ~default=$(drv))

    PRIMITIVE.=
        CC=$(query-parameter $(DRIVER.BYTECODE), c_compiler)
        CFLAGS=$(query-parameter $(DRIVER.BYTECODE), ocamlc_cflags)
        CPPFLAGS=$(query-parameter $(DRIVER.BYTECODE), ocamlc_cppflags)

    # Note Well: these parameters must be split on whitespace into sequences
    PRIMITIVE.CFLAGS=$(split $(PRIMITIVE.CFLAGS))
    PRIMITIVE.CPPFLAGS=$(split $(PRIMITIVE.CPPFLAGS))

    SUFFIX.=
        OBJ=$(query-parameter $(DRIVER.BYTECODE), ext_obj)
        LIB=$(query-parameter $(DRIVER.BYTECODE), ext_lib)
        DLL=$(query-parameter $(DRIVER.BYTECODE), ext_dll)
        EXE=$(query-parameter $(DRIVER.BYTECODE), ext_exe)

    VERSION=$(query-parameter $(DRIVER.BYTECODE), version)
    STDLIB=$(dir $(query-parameter $(DRIVER.BYTECODE), standard_library))

    NATIVE_SUPPORTED=$(if $(DRIVER.NATIVE), true, false)

    DLL_SUPPORTED=
        value $`(query-parameter $(DRIVER.BYTECODE), supports_shared_libraries)

    INCLUDE=$(EMPTY_ARRAY)
    LDSEARCH=$(EMPTY_ARRAY)
    OPTIONS=$(EMPTY)
    CMO_OPTIONS=$(EMPTY)
    CMX_OPTIONS=$(EMPTY)
    LINK_OPTIONS=$(EMPTY)
    C_LIBRARIES=$(EMPTY_ARRAY)
    C_OPTIONS=$(EMPTY)

    ### Filter out options used in "release" modes
    ReleaseModeOptionFilter(options)=
        return $(filter-out -warn-error, $(options))

    ### OCaml dependency scanner auxiliary
    protected.ocamldep-aux()=
        awk (b, $(stdin))
        case $'^\(.*\):[[:space:]]*\(.*\)$'
            target=$1
            depend=$(split $2)
            export target depend
        default
            x.=
                message=$"unmatched ocamldep output $0"
            raise $x
        target=$(STAGE.Ref $(target))
        depend=$(find-ocaml-targets-in-path-optional $(INCLUDE), $(depend))
        println($"$(target): $(depend)")

    ### Make C compiler option prefixes
    protected.compile-primitive-options()=
        opts=$(C_OPTIONS)
        export opts
        if $(equal $(MODE), $"release")
            opts=$(C_family_driver.ReleaseModeOptionFilter $(opts))
        opts=$(mapprefix $"-ccopt", $(opts))
        incs=$(mapprefix $"-I", $(INCLUDE))
        return $(opts) $(incs)

    ### Make module scanner options
    protected.compile-module-options(ext)=
        opts=$(OPTIONS)
        export opts
        if $(equal $(MODE), $"release")
            opts=$(ReleaseModeOptionFilter $(opts))
        switch $(ext)
        case $"cmo"
            opts+=$(CMO_OPTIONS)
        case $"cmx"
            opts+=$(CMX_OPTIONS)
        incs=$(mapprefix $"-I", $(INCLUDE))
        return $"-bin-annot" $(opts) $(incs)

    ### Make module scanner options
    protected.scanner-module-options()=
        inc=$(mapprefix $"-I", $(filter-exists $(INCLUDE)))
        return $"-depend" $"-one-line" $(inc)

    ### Make library linker options
    protected.lib-search-options()=
        uselibs=$(mapprefix $"-l", $(C_LIBRARIES))
        if $(uselibs)
            search=$(mapprefix $"-L", $(LDSEARCH))
            return $(search) $(uselibs)
        else
            return

    ### Make executable linking options
    protected.executable-options()=
        opts=$(OPTIONS)
        export opts
        if $(equal $(MODE), $"release")
            opts=$(ReleaseModeOptionFilter $(opts))
        incs=$(mapprefix $"-I", $(INCLUDE))
        return $(opts) $(incs)

    ### Make bytecode executable linking options
    protected.bytecode-executable-options()=
        return $(executable-options)

    ### Make native executable linking options
    protected.native-executable-options()=
        return $(executable-options)

    ### Common logic for compiling modules
    protected.with-module-rule-actions(driver, ext)=
        private.driver=$(driver)
        private.ext=$(ext)
        this.SEARCH=$(INCLUDE)
        this.scanner-action()=
            return $(driver) $(scanner-module-options) $@ $< | ocamldep-aux
        this.producer-action()=
            return $(driver) $(compile-module-options $(ext)) -o $@ -c $<
        return $(this)

    ### Augment toolchain with MODE options
    WithModeOptions()=
        ret=$(this)
        export ret
        switch $(MODE)
        case $"develop"
            ret.OPTIONS+=-g
            ret.OPTIONS+=$(array -principal -strict-formats -strict-sequence)
            ret.OPTIONS+=$(array -w +A -warn-error A)
            ret.LINK_OPTIONS+=-g
            ret.C_OPTIONS+=$(array -g -Wall -Werror)
            ret.CMO_OPTIONS+=-compat-32
        case $"release"
            ret.OPTIONS+=$(array -unsafe -noassert)
            ret.C_OPTIONS+=$(array -O3 -DNDEBUG)
            ret.CMX_OPTIONS+=$(array -inline 9)
        return $(ret)

    ### Compose a primitive auxiliary library static archive file name...
    ToStubLibFileName(name)=
        name=$(add-wrapper $"lib", $(SUFFIX.LIB), $(name))
        return $(STAGE.Ref $(name))

    ### Compose a primitive auxiliary library shared library file name...
    ToStubDllFileName(name)=
        if $(DLL_SUPPORTED)
            name=$(add-wrapper $"dll", $(SUFFIX.DLL), $(name))
            return $(STAGE.Ref $(name))
        else
            return $(EMPTY)

    ### Compose a bytecode library file name...
    ToBytecodeLibraryFileName(name)=
        return $(STAGE.Ref $(addsuffix $".cma", $(name)))

    ### Compose a native library file name...
    ToNativeLibraryFileName(name)=
        return $(STAGE.Ref $(addsuffix $".cmxa", $(name)))

    ### Compose a native plugin file name...
    ToNativePluginFileName(name)=
        return $(STAGE.Ref $(addsuffix $".cmxs", $(name)))

    ### Compose a native auxiliary library file name...
    ToNativeAuxiliaryLibraryFileName(name)=
        return $(STAGE.Ref $(addsuffix $(SUFFIX.LIB), $(name)))

    ### Compile primitive objects
    Primitives(cfiles)=
        opts=$(EMPTY)
        opts+=$(PRIMITIVE.CFLAGS) $(PRIMITIVE.CPPFLAGS) $(C_OPTIONS)
        opts+=-I$(STDLIB) $(addprefix $"-I", $(INCLUDE))
        this.scanner-action()=
            opts+=$"-MM" $"-MT" $(file $(removesuffix $".scan", $@)) $<
            return $(PRIMITIVE.CC) $(opts)
        this.producer-action()=
            opts+=$"-o" $@ $"-c" $<
            return $(PRIMITIVE.CC) $(opts)
        this.SEARCH=$(INCLUDE)
        this.DEPEND=$(filter-out %.cma %.cmxa %.cmxs, $(DEPEND))
        this.DEPEND=$(filter-out %.cmi %cmo %.cmx, $(DEPEND))
        foreach (i => ..., $(file $(cfiles)))
            o=$(STAGE.Ref $(replacesuffixes $".c", $(SUFFIX.OBJ), $i))
            w=$(ScannerRule ~target=$o, ~source=$i)
            value $(ComposeRule ~target=$o, ~source=$i, ~scanner=$w)

    ### Compile OCaml interface files (.cmi and .cmti)
    Interfaces(mlifiles)=
        this=$(with-module-rule-actions $(DRIVER.BYTECODE), $"cmi")
        ret=$(EMPTY_ARRAY)
        depend=$(filter-out %.cmxa %.cmxs %.cmo %.cmx, $(DEPEND))
        export ret depend
        Shell.ocamldep-aux(_)=
            return $(ocamldep-aux)
        this.DEPEND=
        foreach (mli => ..., $(file $(mlifiles)))
            cmi=$(STAGE.Ref $(replacesuffixes $".mli", $".cmi", $(mli)))
            cmti=$(file $(replacesuffixes $".cmi", $".cmti", $(cmi)))
            w=$(ScannerRule ~target=$(cmi), ~source=$(mli))
            this.DEPEND=$(depend)
            ComposeRule(~target=$(cmi), ~source=$(mli), ~scanner=$w, ~effects=$(cmti))
            depend+=$(cmi)
            ret+=$(cmi)
            ret+=$(cmti)
        return $(ret)

    ### Compile OCaml bytecode module files (.cmo and .cmt)
    BytecodeModules(mlfiles)=
        this=$(with-module-rule-actions $(DRIVER.BYTECODE), $"cmo")
        ret=$(EMPTY_ARRAY)
        Shell.ocamldep-aux(_)=
            return $(ocamldep-aux)
        depend=$(filter-out %.cmxa %.cmxs %.cmo %.cmx, $(DEPEND))
        export ret depend
        foreach (ml => ..., $(file $(mlfiles)))
            cmo=$(STAGE.Ref $(replacesuffixes $".ml", $".cmo", $(ml)))
            ret+=$(cmo)
            cmt=$(file $(replacesuffixes $".cmo", $".cmt", $(cmo)))
            cmi=$(file $(replacesuffixes $".cmo", $".cmi", $(cmo)))
            w=$(ScannerRule ~target=$(cmo), ~source=$(ml))
            effects=$(cmt)
            export effects
            if $(target-is-proper $(cmi))
                depend+=$(cmi)
            this.DEPEND=$(depend)
            if $(not $(target-is-proper $(cmi)))
                effects+=$(cmi)
                depend+=$(cmi)
            ret+=$(effects)
            ComposeRule(~target=$(cmo), ~source=$(ml), ~scanner=$w, ~effects=$(effects))
        return $(ret)

    ### Compile OCaml bytecode module files (.cmo and .cmt)
    NativeModules(mlfiles)=
        this=$(with-module-rule-actions $(DRIVER.NATIVE), $"cmx")
        ret=$(EMPTY_ARRAY)
        Shell.ocamldep-aux(_)=
            return $(ocamldep-aux)
        depend=$(filter-out %.cmxs, $(DEPEND))
        export ret depend
        foreach (ml => ..., $(file $(mlfiles)))
            cmi=$(STAGE.Ref $(replacesuffixes $".ml", $".cmi", $(ml)))
            cmx=$(STAGE.Ref $(replacesuffixes $".ml", $".cmx", $(ml)))
            ret+=$(cmx)
            ofile=$(file $(replacesuffixes $".cmx", $(SUFFIX.OBJ), $(cmx)))
            ret+=$(ofile)
            w=$(ScannerRule ~target=$(cmx), ~source=$(ml))
            depend+=$(cmi)
            this.DEPEND=$(depend)
            ComposeRule(~target=$(cmx), ~source=$(ml), ~scanner=$w, ~effects=$(ofile))
            depend+=$(cmx)
        return $(ret)

    ### Returns the driver command line prefix for composing a library target
    protected.mklib-prefix(driver, target, ofiles)=
        if $(ofiles)
            return $(DRIVER.MKLIB) $(LINK_OPTIONS) -o $(rootname $(target))
        else
            return $(driver) $(LINK_OPTIONS) -a -o $(target)

    ### Returns the effects of composing a library target
    protected.library-effects(ofiles)=
        ret=$(EMPTY_ARRAY)
        export ret
        if $(ofiles)
            ret+=$(ToStubLibFileName $(name))
            if $(DLL_SUPPORTED)
                ret+=$(ToStubDllFileName $(name))
        return $(ret)

    ### Link a bytecode library (.cma)
    BytecodeLibrary(name, cmofiles, ofiles)=
        cmafile=$(ToBytecodeLibraryFileName $(name))
        src=$(cmofiles) $(ofiles)
        eff=$(library-effects $(ofiles))
        this.producer-action()=
            driver=$(mklib-prefix $(DRIVER.BYTECODE), $(cmafile), $(ofiles))
            return $(driver) $+ $(lib-search-options)
        return $(ComposeRule ~target=$(cmafile), ~source=$(src), ~effects=$(eff))

    ### Link a native library (.cmxa)
    NativeLibrary(name, cmxfiles, ofiles)=
        cmxafile=$(ToNativeLibraryFileName $(name))
        libfile=$(ToNativeAuxiliaryLibraryFileName $(name))
        src=$(cmxfiles) $(ofiles)
        eff=$(libfile) $(library-effects $(ofiles))
        this.producer-action()=
            driver=$(mklib-prefix $(DRIVER.NATIVE), $(cmxafile), $(ofiles))
            return $(driver) $+ $(lib-search-options)
        return $(ComposeRule ~target=$(cmxafile), ~source=$(src), ~effects=$(eff))

    ### Link a native plugin (.cmxs)
    NativePlugin(name, cmxfiles, ofiles)=
        cmxsfile=$(ToNativePluginFileName $(name))
        opts=$(lib-search-options)
        driver=$(if $(ofiles), $(DRIVER.MKLIB), $(DRIVER.BYTECODE))
        src=$(cmxfiles) $(ofiles)
        this.producer-action()=
            name=$(rootname $(cmxsfile))
            return $(DRIVER.NATIVE) -shared -o $(cmxsfile) $+ $(opts)
        return $(ComposeRule ~target=$(cmxsfile), ~source=$(src))

    ### Compose a bytecode executable file name...
    ToBytecodeExecutableFileName(name)=
        return $(STAGE.Ref $(addsuffix $".run", $(name)))

    ### Compose a native executable file name...
    ToNativeExecutableFileName(name)=
        return $(STAGE.Ref $(addsuffix $(SUFFIX.EXE), $(name)))

    ### Link a native executable
    BytecodeExecutable(name, cmfiles, ?custom=$(EMPTY))=
        runfile=$(ToBytecodeExecutableFileName $(name))
        opts=$(bytecode-executable-options)
        if $(custom)
            opts=$"-custom" $(opts)
            export opts
        this.producer-action()=
            return $(DRIVER.BYTECODE) $(opts) -o $@ $+
        return $(ComposeRule ~target=$(runfile), ~source=$(cmfiles))

    ### Link a native executable
    NativeExecutable(name, cmfiles)=
        exefile=$(ToNativeExecutableFileName $(name))
        opts=$(native-executable-options)
        this.producer-action()=
            return $(DRIVER.NATIVE) $(opts) -o $@ $+
        return $(ComposeRule ~target=$(exefile), ~source=$(cmfiles))

### End
