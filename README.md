# Conjury — Alternative library for OMake

OMake is a modernized variant of the venerable family of software build tools
descended from the Unix MAKE(1) program. Its standard library is well-suited
for use in small projects, and this alternative library is a framework for
using OMake in building larger projects with many subprojects and complex build
configuration and packaging options.

A distinguishing feature of Conjury is its neutral design, with a basis layer
that is independent of programming language toolchain and application packaging systems. This basis layer is extensible and can support a wide variety of
compiled and interpreted languages.

Currently supported programming languages:

    - C and C++
    - OCaml

Currently supported application packaging systems:

    - Conventional POSIX unmanaged installations
    - OPAM

## Examples

Insert some examples.

## Installing

Prerequisites:

    - ocaml >= 4.08
    - ocamlfind >= 1.7.3
    - omake >= 0.10.3
    - ounit2 >= 2.2

Best to use OPAM to install everything and have the dependencies managed
automatically:

> $ opam install conjury

Or, build OMake manually and install Conjury directly into its library.

## History

An earlier unnumbered version of the Conjury library for OMake exists. It was
designed to be imported as a subrepository or a subtree, and it was never
packaged or tagged with a version number. This library is a total redesign of
that earlier work.

Accordingly, the Conjury library commences its version sequence at 2. Its first
revisions in development were made on a Mercurial named branch "v2".
