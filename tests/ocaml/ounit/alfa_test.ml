open OUnit2

let suite = "suite" >::: [
    "hello" >:: fun _ctxt ->
        let cwd = Unix.getcwd () in
        Printf.eprintf "CWD %s\n" cwd;
        flush stderr;
        Alfa.hello ()
]

let _ = run_test_tt_main suite
