##############################################################################
#
# Copyright (C) 2018-2023, james woodyatt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#   Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

open Conjury/OPAM
open Conjury/Language/OCaml/Findlib
open Conjury/Language/OCaml/OUnit

tools=$(OCaml_findlib_compiler)
tools.PACKAGE+= unix
tools.MLFLAGS+= -w -70

src.=
    ALFA=$(OCaml_source.New ~names=alfa)
    MAIN=$(OCaml_source.New ~names=main)

alfa=
    value $(OCaml_library ~name=alfa, ~source=$(src.ALFA), ~tools=$(tools))

main=
    libs=alfa
    run=$(OCaml_test_program ~mode=bytecode, ~name=main.run, ~source=$(src.MAIN), ~libs=$(libs), ~tools=$(tools))
    opt=$(OCaml_test_program ~mode=native, ~name=main.opt, ~source=$(src.MAIN), ~libs=$(libs), ~tools=$(tools))
    value $(run) $(opt)

docs=
    value $(OCaml_documents ~name=$"test", ~source=$(src.ALFA) $(src.MAIN), ~tools=$(tools))

pkg.=
    extends $(OCaml_findlib_installer.New ~name=$"test", ~version=$"1.0")
    AssertInstance(~obj=$(this.STAGE), ~class=$"Stage", ~where=$"tests/ocaml/OMakefile.pkg")
    DESCRIPTION=$"Test META"
    ARCHIVES=$(alfa)
    REQUIRES=$"unix"
    FILES=$(filter-out $"%.cma" $"%.cmxa", $(alfa))

    foo.=
        extends $(OCaml_findlib_meta.New ~name=$"foo")
        DESCRIPTION=$"Test.Foo META"
        FILES=$(filter $"%.cma" $"%.cmxa", $(alfa))
        REQUIRES=$"bigarray"

    SUBPACKAGES[]=
        $(foo)

    private.meta=$(this.meta-target)
    .PHONY: findlib-lint
    findlib-lint: $(meta)
        ocamlfind lint $(meta)

test: $(pkg.STAGE.Checkpoint ~name=ocaml, ~depend=$(pkg.FILES) $(docs))

# install: $(pkg.Install)
# remove: $(pkg.Remove)

### End
